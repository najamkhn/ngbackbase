# ngbackbase
-----

Hi there,

This project is built using the Angular 1.5.3. There are a couple of libraries I'm using, here's the list of all them:

* jQuery@1.11.0
* Bootstrap@3.3.6
* Lodash@2.4.1
* Angularjs@1.5.3
* Fancybox@2.1.5

To serve all the static content, I'm using HarpJS[1](https://harpjs.com/)

## Setup

To setup and run the project, please just go to the project base directory and run the following command:
```
npm start
```

and then navigate to this URL: [http://localhost:9090](http://localhost:9090/)

---

 [1] I've also contributed to that project.
