angular.module('ngBackbase', [])
    .controller('MainCtrl', function ($scope, $http) {
        $scope.years = _.range(2005, 2015);

        $scope.yearClicked = function(year) {
          $scope.getByYear(year);
          $scope.getAllSeasonsByYear(year);
        }


        $scope.getByYear = function(year) {
          $http({
            method: 'GET',
            url: 'http://ergast.com/api/f1/' + year + '/driverStandings.json?limit=5'
          }).then(function(response) {

            var standlingLists = response.data.MRData.StandingsTable.StandingsLists;
            $scope.driversList = standlingLists[0].DriverStandings;

            $scope.season = response.data.MRData.StandingsTable.season;

            // save the driver with highest ranking
            $scope.seasonWinner = $scope.driversList[0].position == '1' && $scope.driversList[0].Driver.driverId;

          }, function() {
            console.log('error');
          });
        }


        $scope.getAllSeasonsByYear = function(year) {
          $http({
            method: 'GET',
            url: 'http://ergast.com/api/f1/' + year + '.json?limit=12'
          }).then(function(response) {
            $scope.racesList = response.data.MRData.RaceTable.Races;
          }, function() {
            console.log('error');
          });
        }

        $scope.getSeasonDetails = function(season, round) {
          $http({
            method: 'GET',
            url: 'http://ergast.com/api/f1/' + season + '/' + round + '/results.json?limit=10'
          }).then(function(response) {

            $scope.results = response.data.MRData.RaceTable.Races[0].Results;
            var winner = $scope.seasonWinner;
            var items = [];

            var list = _.map($scope.results, function(index) {
              var isChampion = 'isChampion=' + (index.Driver.driverId == winner);
              var item = '<li class="" ' + isChampion +'>' + index.Driver.givenName + ' ' + index.Driver.familyName  + '</li>';
              items.push(item);
              return item;
            }).join('');


            $.fancybox($('#modalHtml').html() + list, {
              'autoWidth': true
            });

          }, function() {
            console.log('error');
          });

        }
    })
;
